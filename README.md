# Kube Vault

## Quick start

Create persistent volume directory within glusterfs.
```bash
gluster volume create vault replica 4 192.168.0.2{1..4}:/mnt/usb1/vault
```

An official image for ARM64 architecture exists for hashicorp/vault.
For more details, check here [docker hub](https://hub.docker.com/r/hashicorp/vault).

```bash
kubectl apply -f vault-datastorage.yaml
kubectl apply -f vault-namespace.yaml
helm -n vault install vault hashicorp/vault -f values.yaml
kubectl -n vault exec --stdin=true --tty=true vault-0 -- vault operator init
kubectl -n vault exec --stdin=true --tty=true vault-0 -- vault operator unseal <Unseal Key 1>
kubectl -n vault exec --stdin=true --tty=true vault-0 -- vault operator unseal <Unseal Key 2>
kubectl -n vault exec --stdin=true --tty=true vault-0 -- vault operator unseal <Unseal Key 3>
```

## Removal

```bash
helm -n vault uninstall vault hashicorp/vault
kubectl delete -f vault-namespace.yaml
kubectl delete -f vault-storage.yaml
```
